#!/usr/bin/env python

from sys import argv, platform
from os import path, popen
from random import randrange , seed
from operator import itemgetter
from time import sleep
from optparse import OptionParser
        
#search path for gnuplot executable 
#be careful on using windows LONG filename, surround it with double quotes.
#and leading 'r' to make it raw string, otherwise, repeat \\.

#a simple gnuplot object
class gnuplot:
    def __init__(self, term='onscreen'):
        # -persists leave plot window on screen after gnuplot terminates
        if platform == 'win32':
            cmdline = gnuplot_exe
            self.__dict__['screen_term'] = 'windows'
        else:
            cmdline = gnuplot_exe + ' -persist'
            self.__dict__['screen_term'] = 'x11'
        self.__dict__['iface'] = popen(cmdline,'w')
        self.set_term(term)

    def set_term(self, term):
        if term=='onscreen':
            self.writeln("set term %s" % self.screen_term)
        else:
            #term must be either x.ps or x.png
            if term.find('.ps')>0:
                self.writeln("set term postscript eps color 22")
            elif term.find('.png')>0:
                self.writeln("set term png")
            else:
                print("You must set term to either *.ps or *.png")
                raise SystemExit
            self.output = term
        
    def writeln(self,cmdline):
        self.iface.write(cmdline + '\n')

    def __setattr__(self, attr, val):
        if type(val) == str:
            self.writeln('set %s \"%s\"' % (attr, val))
        else:
            print("Unsupport format:", attr, val)
            raise SystemExit

    #terminate gnuplot
    def __del__(self):
        self.writeln("quit")
        self.iface.flush()
        self.iface.close()

    def __repr__(self):
        return "<gnuplot instance: output=%s>" % term

    #data is a list of [x,y]
    def plotline(self, data):
        self.writeln("plot \"-\" notitle with lines linewidth 1")
        for i in range(len(data)):
            self.writeln("%f %f" % (data[i][0], data[i][1]))
            sleep(0) #delay
        self.writeln("e")
        if platform=='win32':
            sleep(3)

def plot_roc(deci, label):
    #count of postive and negative labels
    db = []
    pos, neg = 0, 0         
    for i in range(len(label)):
        if label[i]>0:
            pos+=1
        else:    
            neg+=1
        db.append([deci[i], label[i]])

    #sorting by decision value
    db = sorted(db, key=itemgetter(0), reverse=True)

    #calculate ROC 
    xy_arr = []
    tp, fp = 0., 0.            #assure float division
    for i in range(len(db)):
        if db[i][1]>0:        #positive
            tp+=1
        else:
            fp+=1
        xy_arr.append([fp/neg,tp/pos])

    #area under curve
    aoc = 0.            
    prev_x = 0
    for x,y in xy_arr:
        if x != prev_x:
            aoc += (x - prev_x) * y
            prev_x = x
    return '%.5f' %(aoc)

def read_score_label(file_name) :
    score = []
    label = []
    npos, nneg = 0, 0
    fh = open(file_name, 'r')
    for line in fh :
        line   = line.rstrip()
        items  = line.split()
        if len(items) == 2 :
            (s, l) = items
            score.append(float(s))
            label.append(int(l))
            if int(l) > 0 :
                npos += 1
            else :
                nneg += 1
    fh.close()
    #print "Read %d positive samples, %d negative samples.\n" %(npos, nneg)
    return (score, label)

def main(opts, args):
    #get decision value, with positive = label+
    (score, label) = read_score_label(opts.prediction)
    return plot_roc(score, label)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--prediction", dest="prediction",
            type="str",
            help = "file contains a prediction file of score and label: score<tab>label",
            default="./prediction.txt")
    (opts, args) = parser.parse_args()

    res = main(opts, args)
    print res
