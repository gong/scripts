#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################
# 
# Copyright (c) 2016 All Rights Reserved
# 
########################################################################
 
"""
File: text2feat.py
Author: gong(li.gong@outlook.com)
Date: 2016/11/27 20:25:39
"""
import sys
import argparse
import codecs

class InstanceBuilder(object):
    def __init__(self, weighted, binarize, fdict):
        self.fdict = fdict
        self.binarize = binarize
        if weighted:
            self.processor = self.processWeightedTokenFeat
        else:
            self.processor = self.processTokenFeat
             
    def convert(self, data_str):
        fields = data_str.split()
        if len(fields) < 2:
            print >> sys.stderr, 'input format error'
        info = fields[0]
        feats = {}
        for feature in fields[1:]:
            fid, fvalue = self.processor(feature)
            if fid not in feats:
                feats[fid] = 0.0
            feats[fid] += fvalue
        if self.binarize:
            feat_field = ' '.join(['%d:1' % k for k, v in sorted(feats.items(), key = lambda x:x[0])])
        else:
            feat_field = ' '.join(['%d:%.2f' % (k, v) for k, v in sorted(feats.items(), key = lambda x:x[0])])
        return '%s\t%s' % (info, feat_field)

    def processTokenFeat(self, token):
        if token not in self.fdict:
            self.fdict[token] = len(self.fdict) + 1
        return self.fdict[token], 1.0
    
    def processWeightedTokenFeat(self, weightedToken):
        fields = weightedToken.split(':')
        if len(fields) != 2:
            print >> sys.stderr, 'Feature format error.'
            sys.exit(1)
        fname, fvalue = fields
        if fname not in self.fdict:
            self.fdict[fname] = len(self.fdict) + 1
        return self.fdict[fname], float(fvalue)
    
if __name__ == '__main__':
    readme = """
    This script receives input from sys.stdin, and convert the input string into instance.
    Two formats are accepted:
    1.[token-format]: <info> <fname> <fname> ...
    2.[weighted-format]: <info> <fname>:<fvalue> <fname>:<fvalue> ... 
    By default, the output feature value is the sum of weight (or count) of all the occurrance of 
    the feature in the same instance. If --binarize is given, then the feature weight is set to be
    1 if it occurs.
    """
    parser = argparse.ArgumentParser(prog='text2feat', description=readme)
    parser.add_argument("--weighted", action='store_true', \
        help = "True if the input features are weighted as format <fname>:<fweight>")
    parser.add_argument("--binarize", action='store_true', \
        help = "True if you want to omit the feature weight")
    parser.add_argument("--encoding", default='utf-8', help = "the I/O encoding (utf-8 by default)")
    parser.add_argument("--dict", default='fdict.txt', help = "The output feature dict")
    args = parser.parse_args()

    feat_dict = {}
    ins_builder = InstanceBuilder(args.weighted, args.binarize, feat_dict)
    for line in sys.stdin:
        line = line.strip().decode(args.encoding) 
        inst_str = ins_builder.convert(line)
        print inst_str.encode(args.encoding)

    outf = codecs.open(args.dict, 'w', args.encoding)
    for fname, fid in sorted(feat_dict.items(), key = lambda x:x[1]):
        outf.write('%d\t%s\n' % (fid, fname))
    outf.close()
    

