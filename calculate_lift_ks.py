#!/usr/bin/env python
# -*- coding: utf-8 -*-
########################################################################
# 
# Copyright (c) 2016 All Rights Reserved
# 
########################################################################
 
"""
File: calculate_lift_ks.py
Author: gong(gongli@baidu.com)
Date: 2016/11/27 20:25:39
"""
import sys
import argparse
import codecs

if __name__ == '__main__':
    readme = """
    """
    parser = argparse.ArgumentParser(prog='calculate_lift_ks.py', description=readme)
    parser.add_argument('-d', "--data", required=True, help = "Model predict result. Format: score<tab>label")
    args = parser.parse_args()

    pos_count = 0
    neg_count = 0
    pred_scores = []
    for line in open(args.data, 'r'):
        fields = line.strip().split()
        if len(fields) != 2:
            print >> sys.stderr, 'Error: wrong format', line
            sys.exit(0)
        score, label = fields
        pred_scores.append((float(score), label))
        if label == '1':
            pos_count += 1
        elif label == '0':
            neg_count += 1
        else:
            print >> sys.stderr, "Error: the label has to be 0 or 1."
            sys.exit(0)

    if len(pred_scores) < 10:
        print >> sys.stderr, "Error: too less data."
        sys.exit(0)

    total_num = len(pred_scores)
    global_pos_ratio = float(pos_count) / (pos_count + neg_count)
    label_count = {}
    label_count['0'] = 0
    label_count['1'] = 0
    check_points = set([int(total_num * v / 10.0) for v in range(1, 11)])
    print 'Top percent\tLift\tKS'
    for idx, (score, label) in enumerate(sorted(pred_scores, key=lambda x:x[0], reverse=True)):
        label_count[label] += 1
        pos_ratio = label_count['1'] * 1.0 / (idx + 1)
        lift = pos_ratio / global_pos_ratio
        ks_good = label_count['1'] * 100.0 / pos_count
        ks_bad = label_count['0'] * 100.0 / neg_count
        ks_diff = abs(ks_good - ks_bad)
        if idx in check_points:
            print '%d percent\t%.2f\t%.2f' % (int(idx * 100.0 / total_num), lift, ks_diff)
    
